﻿namespace ToDone.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using Models;

    public class TaskController : Controller
    {
        //TODO: MongoDB
        private static List<Task> _tasks = new List<Task>
        {
            new Task("Homework") { CreatedAt = new DateTime(2014, 10, 24, 13, 45, 58), Number = 1 },
            new Task("Book") { CreatedAt = new DateTime(2015, 1, 14, 16, 46, 22), Number = 2 },
            new Task("Clean") { CreatedAt = new DateTime(2015, 2, 5, 5, 56, 12), Number = 3 }
        };

        public ActionResult Index()
        {
            return View(_tasks);
        }

        public ActionResult Create(string name)
        {
            _tasks.Add(new Task(name));

            return PartialView("Partials/TodoList", _tasks);
        }

        public EmptyResult Delete(Guid id)
        {
            _tasks.Remove(_tasks.Find(x => x.Id == id));

            return null;
        }
    }
}