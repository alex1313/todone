﻿namespace ToDone
{
    using System.Web.Optimization;

    public static class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new LessBundle("~/Content/bootstrap/less")
                .Include("~/Content/bootstrap/bootstrap.less"));

            bundles.Add(new LessBundle("~/Content/less")
                .Include("~/Content/site.less"));

            bundles.Add(new ScriptBundle("~/Scripts/knockout")
                .Include("~/Scripts/knockout-{version}.js"));

            bundles.Add(new ScriptBundle("~/Scripts/jquery")
                .Include("~/Scripts/jquery-{version}.js")
                .Include("~/Scripts/jquery.unobtrusive-ajax.js"));

            bundles.Add(new ScriptBundle("~/Scripts/site")
                .Include("~/Scripts/index.js")
                .Include("~/Scripts/todo-list.js"));
        }
    }
}