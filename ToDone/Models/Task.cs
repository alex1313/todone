﻿namespace ToDone.Models
{
    using System;

    public class Task
    {
        public Task(string name)
        {
            Id = Guid.NewGuid();
            Name = name;
            CreatedAt = DateTime.Now;
        }

        public Guid Id { get; set; }
        public int Number { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public bool Completed { get; set; }
    }
}